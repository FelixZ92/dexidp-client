FROM alpine
RUN mkdir /application

ADD build/dexidp-client /application/dexidp-client

ENTRYPOINT [ "/application/dexidp-client" ]
