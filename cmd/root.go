// Package cmd /*
/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/dexidp/dex/api/v2"
	"github.com/spf13/cobra"
)

var (
	cfgFile   string
	caCrt     string
	clientCrt string
	clientKey string
	endpoint  string
)

var dexClient api.DexClient

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "dexidp-client",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.dexidp-client.yaml)")
	rootCmd.PersistentFlags().StringVar(&caCrt, "ca-crt", "", "CA certificate")
	rootCmd.PersistentFlags().StringVar(&clientCrt, "client-crt", "", "Client certificate")
	rootCmd.PersistentFlags().StringVar(&clientKey, "client-key", "", "Client key")
	rootCmd.PersistentFlags().StringVar(&endpoint, "endpoint", "127.0.0.1:5557", "dex grpc endpoint")

	_ = cobra.MarkFlagRequired(rootCmd.PersistentFlags(), "ca-crt")
	_ = cobra.MarkFlagRequired(rootCmd.PersistentFlags(), "client-crt")
	_ = cobra.MarkFlagRequired(rootCmd.PersistentFlags(), "client-key")
}
