// Package cmd /*
/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/dexidp/dex/api/v2"
	"gitlab.com/felixz92/dexidp-client/pkg/dex"

	"github.com/spf13/cobra"
)

var client api.Client

// createClientCmd represents the client command
var createClientCmd = &cobra.Command{
	Use:   "client",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	ValidArgs: []string{"name"},
	Args:      cobra.ExactArgs(1),
	PreRun: func(cmd *cobra.Command, args []string) {
		for i, arg := range args {
			fmt.Printf("run: arg%d = %s\n", i, arg)
		}
		fmt.Printf("run: args len = %d\n", len(args))
		fmt.Printf("run: args = %v\n", args)
		client.Name = args[0]
		client.Id = args[0]
	},
	Run: func(cmd *cobra.Command, args []string) {
		for i, arg := range args {
			fmt.Printf("run: arg%d = %s\n", i, arg)
		}
		fmt.Printf("run: args len = %d\n", len(args))
		fmt.Printf("run: args = %v\n", args)
		fmt.Printf("run: c = %v\n", client)
		dexClient = InitDexClient()
		cobra.CheckErr(dex.CreateClient(dexClient, client))
	},
}

func bindFlags(c *api.Client) {
	createClientCmd.Flags().StringVar(&c.Secret, "secret", "", "")
	createClientCmd.Flags().StringArrayVar(&c.RedirectUris, "redirect-uris", []string{}, "")
	createClientCmd.Flags().StringArrayVar(&c.TrustedPeers, "trusted-peers", []string{}, "")
	createClientCmd.Flags().BoolVar(&c.Public, "public", false, "")
}

func init() {
	createCmd.AddCommand(createClientCmd)

	client = api.Client{}
	bindFlags(&client)
}
