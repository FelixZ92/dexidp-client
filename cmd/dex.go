package cmd

import (
	"github.com/dexidp/dex/api/v2"
	"github.com/spf13/cobra"
	"gitlab.com/felixz92/dexidp-client/pkg/dex"
)

func InitDexClient() api.DexClient {
	client, err := dex.NewDexClient(endpoint, caCrt, clientCrt, clientKey)
	cobra.CheckErr(err)
	return client
}
