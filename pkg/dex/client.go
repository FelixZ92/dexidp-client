package dex

import (
	"context"
	"fmt"
	"github.com/dexidp/dex/api/v2"
	"log"
)

func CreateClient(cli api.DexClient, client api.Client) error {
	createReq := &api.CreateClientReq{Client: &client}

	resp, err := cli.CreateClient(context.TODO(), createReq)
	if err != nil {
		return fmt.Errorf("failed to create client: %v", err)
	}

	if resp.AlreadyExists {
		return fmt.Errorf("client %s already exists", createReq.Client.Name)
	}

	log.Printf("Created client %s with id %s", createReq.Client.Name, resp.Client.Id)

	return nil
}
