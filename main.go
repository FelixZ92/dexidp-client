/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"gitlab.com/felixz92/dexidp-client/cmd"
)

func main() {
	//caCrt := flag.String("ca-crt", "", "CA certificate")
	//clientCrt := flag.String("client-crt", "", "Client certificate")
	//clientKey := flag.String("client-key", "", "Client key")
	//endpoint := flag.String("endpoint", "127.0.0.1:5557", "dex grpc endpoint")
	//flag.Parse()
	//
	//if *clientCrt == "" || *caCrt == "" || *clientKey == "" {
	//	log.Fatal("Please provide CA & client certificates and client key. Usage: ./client --ca-crt=<path ca.crt> --client-crt=<path client.crt> --client-key=<path client key>")
	//}
	//
	//client, err := dex.NewDexClient(*endpoint, *caCrt, *clientCrt, *clientKey)
	//if err != nil {
	//	log.Fatalf("failed creating dex client: %v ", err)
	//}
	//
	//if err := dex.CreatePassword(client); err != nil {
	//	log.Fatalf("testPassword failed: %v", err)
	//}
	cmd.Execute()
}
